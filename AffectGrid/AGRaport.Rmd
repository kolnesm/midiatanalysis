---
title: "Affect Grid Raport"
author: "Martin Kolnes"
date: "28 August 2015"
output: 
        html_document
options: encoding = "UTF-8""

---
**Terve kood**
https://bitbucket.org/kolnesm/midiatanalysis/src/7949c9690c6ac6aabd68f9166b806830d9d8e991/AffectGrid/AffectGridAnalysis.Rmd?at=master&fileviewer=file-view-default


```{r vajalikud paketid, warning= FALSE, message=FALSE, chunk = TRUE, echo = FALSE}
library(dplyr)
library(plyr)
library(reshape2)
library(psych)
library(ggplot2)
library(xtable)
library(pastecs)
library(Gmisc)
library(data.table)
library(plotly)
library(devtools)
library(knitr)
suppressPackageStartupMessages(library(googleVis))
```

```{r toorandmete laadimine, echo = FALSE}
setwd("C:/Users/Martin/Documents/Bitbucket/MIDIATAnalysis/AffectGrid")

t_affect_raw <- read.delim("Inquisit_Data_15_09_14.iqdat", sep="\t", encoding = "UTF-8")

t_affect_raw <- select(t_affect_raw, date, time,
                      subject, gender = radiobuttons.gender.response,
                      age = textbox.age.response,
                      education = dropdown.education.response,
                      language = dropdown.language.response,
                      skill = slider.languageskill.response,
                      block = blockcode, trial = trialcode,
                      stimuli = stimulusitem2,
                      x_percent = values.percent_horizontal_targetx, 
                      y_percent = values.percent_vertical_targety)
```

```{r, echo = FALSE}
# **Andmete puhastamine**
# ----------------------
# **Pooleli jäänud katsete ja harjutusvoorude eemaldamine**
#sum(table(t_affect_raw$time)<191)#lÃµpetamata katseid
t_affect_raw <- ddply(t_affect_raw, "time", function(d) {if(nrow(d)==191) d else NULL})
t_affect_raw <- t_affect_raw[t_affect_raw$time != 0,]
#Harjutusvoorude eemaldamine
t_affect_data <- filter(t_affect_raw, block != "practiceStimuli",
                      trial != "ITI" )
```

```{r muutujad skaalade teisendamiseks, chunk = TRUE, echo = FALSE}
# **Muutujad skaalade teisendamiseks**
valentsNull <- 50.4
valentsNegMax <- 9.8
valentsPosMax <- 91
intenMin <- 87
intenMax <- 33.1
vaheValents <- valentsPosMax - valentsNegMax
vaheInt <- abs(intenMax - intenMin)
```


```{r andmete Töötemine , echo = FALSE}
# **Teisendamine**
# Valem skaala teisendamiseks: (x-min(x))/(max(x) - min(x)) * 100
# Valentsi puhul kasutan miinimumina valentNull muutujate, sest tahan, et neg. tulemsued
# oleksid miinus mÃ¤rgiga.

# Using the mutate() function - http://www.r-bloggers.com/comparing-transformation-styles-attach-transform-mutate-and-within/
t_affect_data <- mutate(t_affect_data, x_percent = ((x_percent-valentsNull)/(valentsPosMax - valentsNull) * 100), y_percent = ((y_percent-intenMin)/(intenMax - intenMin) * 100))
#describeBy(t_affect_data$x_percent, t_affect_data$block)
#describeBy(t_affect_data$y_percent, t_affect_data$block)
```


```{r muutujate lisamine, echo = FALSE}
# **Muutujate lisamine**
#ID
#Vanused kategooriatesse (uus veerg)
#Meeleolu (veergudena)

#Lisan andmetesse(t_affect_data) muutuja(ID), mis nÃ¤itab erinevaid katseisikuid:
newvalues <- seq(1:nrow(count(t_affect_data, "time")))
oldvalues <-  droplevels(count(t_affect_data, "time")$time)
t_affect_data$ID <- newvalues[match(t_affect_data$time, oldvalues)]
#Vanused kategooriatesse:
t_affect_data$age.cat[t_affect_data$age >= median(t_affect_data$age)]  <- 2
t_affect_data$age.cat[t_affect_data$age < median(t_affect_data$age)]  <- 1
```

```{r ,echo = FALSE}
# Meeleolu veergude loomine:
t_affect_mood <- filter(t_affect_data, block == "moodStart"|block == "moodEnd")
# Teen meeleolu vÃ¤Ã¤tustest veerud
t_mood_end <- dcast(t_affect_mood, ID ~ block, value.var = 'x_percent')
t_mood_start <- dcast(t_affect_mood, ID ~ block, value.var = 'y_percent')
t_mood_end <- rename(t_mood_end, c("moodEnd" = "moodEndx", "moodStart"="moodStartx"))
t_mood_start <- rename(t_mood_start, c("moodEnd" = "moodEndy", "moodStart"="moodStarty"))

# Uus tabel katseisikute adnmetega:
# Informatsioon katseisikute kohta:
subjects <- select(t_affect_data, ID, subject, age, gender, education, language, skill )
subjects <- unique(subjects)
subjects <- join(subjects,t_mood_end ,by = "ID")
subjects <- join(subjects,t_mood_start ,by = "ID")

# Trialite ja meeleolude liitmine(lisan meeleolu veerud Ã¼ksikvastuste andmestikku)
t_subject <- select(subjects, ID, moodStartx, moodStarty, moodEndx, moodEndy)

subjects_trials <- join(filter(t_affect_data, block == "determinecoordinatesAll"), t_subject, by ="ID")


#Katseid kokku:
KI <- sum(table(subjects_trials$ID) == 87)
```



```{r, echo = FALSE}
# **Eemaldan andmestikust ekstreemväärtused**  
# VÃ¤Ã¤rtused, mis on kÃµvasti vÃ¤ljaspool raamistikku.
#Kui palju tulemusi on vÃ¤ljaspoole piire:
# table(subjects_trials$x_percent > 120)
# table(subjects_trials$x_percent < -120)
# 
# table(subjects_trials$y_percent > 120)
# table(subjects_trials$y_percent < -20)
#Eemaldan väärtused, mis on väga tugeva kõrvalekaldega

subjects_trials$x_percent[which(subjects_trials$x_percent > 120)]<- NA
subjects_trials$x_percent[which(subjects_trials$x_percent < -120)]<- -NA
subjects_trials$y_percent[which(subjects_trials$y_percent > 120)]<- NA
subjects_trials$y_percent[which(subjects_trials$y_percent < -20)]<- NA

# VÃµrdsustan kõrvalekalded lÃ¤hima skaala väärtusega:
subjects_trials$x_percent[which(subjects_trials$x_percent > 100)]<- 100
subjects_trials$x_percent[which(subjects_trials$x_percent < -100)]<- -100
subjects_trials$y_percent[which(subjects_trials$y_percent > 100)]<- 100
subjects_trials$y_percent[which(subjects_trials$y_percent < 0)]<- 0

```



```{r, echo = FALSE}
# **ANALÜÜS**
# -----------
# **Hinnangute analüüs**
# -----------------------
# **Standardiseerin katseisikute andmed (KI siseselt)**

subjects_trials <- ddply(subjects_trials, c("ID"), transform, x_z_score = scale(x_percent),y_z_score = scale(y_percent))
# 
# ggplot(filter(subjects_stand, ID == 1), aes(x_percent, y_percent, fill = trial, col = trial))+
#         geom_point()+
#         ggtitle("stand")+
#         labs(x = "Valents", y= "Intensiivsus")
```



```{r, echo = FALSE}

# **Andmete puhastamine** - peaks tegema standardiseeritud andmetega?
# -----------------------
# **Katseisikute andmete puhastamine**
# Vastustes puudub vaheldus (lack of variability - (i.e.,providing the same rating for all words in the list) (Warriner et al.,2013)
# kuidas seda testida?
# eemaldada katseisikud, kelle kategooriate vahel ei ole statistiliselt olulist erinevust?

#--------------------
#Valents
# Kontrollin kas kategooriate vahel on katseisikute siseselt erinevusi:
subjects_trials <- droplevels(subjects_trials)
# p <- NULL
# for (i in 1:length(unique(subjects_trials$ID))){
#         #p <- anova(lm(x_percent~trial, filter(subjects_trials, ID == i)))$"Pr(>F)"[1]
#         p <- kruskal.test(x_z_score ~ trial, data = filter(subjects_trials, ID == i))$p.value
#         if (p > 0.05){
#                 # print (i)
#         }
# }
# #--------------------
# 
# for (i in 1:length(unique(subjects_trials$ID))){
#         #p <- anova(lm(y_percent~trial, filter(subject_trials, ID == i)))$"Pr(>F)"[1]
#         #p <- kruskal.test(y_percent ~ trial, data = filter(subjects_trials, ID == i))$p.value
#         if (kruskal.test(y_z_score ~ trial, data = filter(subjects_trials, ID == i))$p.value > 0.05){
#                 # print(i)
#         }
# }
# #--------------------
# ggplot(filter(subjects_trials, ID == 60), aes(x_z_score, y_z_score, fill = trial, col = trial))+
#         geom_point()+
#         labs(x = "Valents", y= "Intensiivsus")
```

```{r, echo = FALSE}
# **Tabel keskmiste tulemustega** - pÃ¤rast korrealtsioonide arvutamist teen selle tabeli uuesti
# Eemaldan Ã¼he KI andmed
# Tulemused sÃµnade kohta
affect_results <- ddply(subjects_trials, .(stimuli, trial), summarize, Vkeskmine = mean(x_percent), Ikeskmine = mean(y_percent), SD.V = sd(x_percent), SD.I = sd(y_percent), ZVkeskmine = mean(x_z_score), ZIkeskmine = mean(y_z_score),SD.VZ = sd(x_z_score), SD.IZ = sd(y_z_score))
# head(affect_results)
# describe(affect_results[,3:6])
```

```{r, echo = FALSE}
# **Lisan vastuse korrelatsiooni keskmise tulemusega**
# Vastajad kelle korrelatsioon keskmiste tulemustega on alla .10 jÃ¤tan vÃ¤lja

#  Katseisiku vastuste korrelatsioon sÃµnade keskmiste tulemustega
# VÃµtan korrelatsioon katseisiku kÃµigi tulemuste ja arvutatud keskmiste tulemuste vahel.
#1) Saan kasutada join() funktsiooni
cor <- join(subjects_trials, affect_results, by = "stimuli")
#2) Lisan veeru korrelatsioonidega:
cor_table <- ddply(na.omit(cor), .(ID), summarize, corrXZ = cor(x_z_score, ZVkeskmine), corrYZ = cor(y_z_score, ZIkeskmine))  
subjects <- join(subjects, cor_table, by = "ID")
```

```{r, echo = FALSE}
# Nii korrelatsioonitabelist kui ka kategooriate vaheliste erinevuste olemasolu uurimine nÃ¤itas, et vÃµib kÃµrvale jÃ¤tta ID 56

#--------------------
#Eemaldan 56. katseisiku andmed:
x_subjects_trials <- subjects_trials
x_subjects <- subjects

subjects_trials <- filter(subjects_trials, ID != 56)
subjects <- filter(subjects, ID != 56)
#--------------------
#NÃµrga keeleoskusega KI jÃ¤tan vÃ¤lja, keeleoskus skaalal 1 kuni 7 vÃ¤iksem kui 3
#Katseisikute enese hinnatud keeleoskus:
#table(subjects$skill)
#Alla 3 ei ole Ã¼htegi tulemust. 
```


```{r, echo = FALSE}
# **Tabel keskmiste tulemustega**
# Tulemused sÃµnade kohta
affect_results <- ddply(subjects_trials, .(stimuli, trial), summarize, Vkeskmine = mean(x_percent,na.rm=TRUE), Ikeskmine = mean(y_percent,na.rm=TRUE), SD.V = sd(x_percent,na.rm=TRUE), SD.I = sd(y_percent,na.rm=TRUE), ZVkeskmine = mean(x_z_score,na.rm=TRUE), ZIkeskmine = mean(y_z_score,na.rm=TRUE),SD.VZ = sd(x_z_score,na.rm=TRUE), SD.IZ = sd(y_z_score,na.rm=TRUE))
# head(affect_results)
# describe(affect_results[,3:6])
```

```{r andmestike puhastamine, echo = FALSE}
# Kujundid vÃ¤lja
subjects_trials <- filter(subjects_trials, trial != "Shapes")
affect_results <- filter(affect_results, trial != "Shapes")
```


```{r, echo = FALSE}
#Kokku kolm andmestikku:
# 1. Kõik andmed koos. toorandmed, z skoorid, katseisiku andmed jne.
# 2. Katseisikute andmed
# 3. Andmed iga sÃµna kohta - keskmised tulemsued jne

# Vormistan tabelid:
subjects_trials <- subset(subjects_trials, select = c(ID, subject, date, time, age, age.cat, gender, education, language, skill, trial, stimuli, x_percent, y_percent, x_z_score, y_z_score, moodStartx, moodStarty, moodEndx, moodEndy))

```


```{r, echo = FALSE}
# Andmestikud 
# subjects - vastajate andmed; viimased kaks veergu on korrelatsioon vastaja hinnangute ja keskmiste vahel. Selle alusel oli ühe vastaja tulemused väiksema korrelatsiooniga kui .10. 
# subjects_trials - kõik tulemused koos
# affect_results - sõnade keskmised tulemused ja standardhÃ¤lbed (arvutatud nii z skooride kui ka tavaliste skooride põhjal)

# Vastuste ja keskmiste korrelatsiooni ning kategooriate erinevuse esinemise alusel jätsin välja ühe vastaja andmed. Kategooriad ei erinenud olulisel määral üksteisest. Vastuste korrelatsioon sÃµnde keskmiste tulemustega oli alla .10.
```

**Valim**
---------
```{r, echo = FALSE}
ssum <- summary(select(subjects, age, gender, education, language, skill))
ssum
```

**Keskmised hinnangud**
---------------------
```{r ,echo = FALSE}
#Üldised keskmised
#Kas peaksin kasutama andmestikkue, kus on üksikud mõõtmised vÃµi andmestikku, kus on juba keskmised?
# affect_results <- droplevels(affect_results)
# 
# describeBy(affect_results[,3:6],affect_results$trial) 
# describeBy(affect_results[,7:10],affect_results$trial) 
# Kasutatud üksikud mõõtmised:
# subjects_trials <- droplevels(subjects_trials)
# #vt <- tapply(subjects_trials$x_percent, subjects_trials$trial, describe)
# vtable <- describeBy(subjects_trials$x_percent, subjects_trials$trial)
# vmat <- as.matrix(round((do.call(rbind.data.frame, vtable)), digits = 2))
# htmlTable(vmat, caption="VALENTS",col.columns = c("none", "#F7F7F7"),  css.cell = "padding-left: .7em; padding-right: .7em;",  tfoot="&dagger; Skaala: -100 kuni 100")  
# 
# vtable <- describeBy(subjects_trials$y_percent, subjects_trials$trial)
# vmat <- as.matrix(round((do.call(rbind.data.frame, vtable)), digits = 2))
# htmlTable(vmat, caption="INTENSIIVSUS",col.columns = c("none", "#F7F7F7"),  css.cell = "padding-left: .7em; padding-right: .7em;",  tfoot="&dagger; Skaala: 0 kuni 100")  
```

```{r}
## Kasutatud andmestik - hinnangute keskmised
vtable <- describeBy(affect_results[,7:8],affect_results$trial) 
vmat <- as.matrix(round((do.call(rbind.data.frame, vtable)), digits = 2))
htmlTable(vmat, caption="HINNANGUD",col.columns = c("none", "#F7F7F7"),  css.cell = "padding-left: .7em; padding-right: .7em;", tfoot="&dagger; V - valents; I - intensiivsus; Z - standardiseeritud tulemustega arvutatud") 
```
  

**Sõnade valik**
----------------
```{r}
A <- filter(affect_results, stimuli != "ravi" , stimuli != "vabadus", stimuli != "kirg" ,stimuli != "vaikus", stimuli != "sõber", stimuli != "meeldiv", stimuli != "ime", stimuli != "võit", stimuli != "energia", stimuli != "selgus")

A <- filter(A, stimuli != "igav" , stimuli != "halvem", stimuli != "kaebus" ,stimuli != "kole", stimuli != "nõrkus", stimuli != "paha", stimuli != "surve", stimuli != "ehmatus", stimuli != "ohtlik", stimuli != "relv")

A <- filter(A, stimuli != "rahulik" , stimuli != "padi", stimuli != "uni" ,stimuli != "sujuv", stimuli != "pilv", stimuli != "küla", stimuli != "lihtne", stimuli != "ümar", stimuli != "unustus", stimuli != "uinutaja")

A <- filter(A, stimuli != "mõõk" , stimuli != "kihv", stimuli != "nõel" ,stimuli != "nuga", stimuli != "irvitus", stimuli != "tormine", stimuli != "äge", stimuli != "erutus", stimuli != "kirurg", stimuli != "süda")

affect_results <- A
# torm või tormine
#võimas ja raev sisse.

# A <- filter(affect_results, SD.VZ < quantile(affect_results$SD.VZ, c(.90)), SD.IZ < quantile(affect_results$SD.IZ, c(.90)))
# AH <- subset(filter(A, trial == "High"), ZIkeskmine > 0)
# AL <- subset(filter(A, trial =="Low"),  ZIkeskmine < 0)
# AN <- subset(filter(A, trial == "Negative"), ZVkeskmine < 0)
# AP <- subset(filter(A, trial == "Positive"), ZIkeskmine > 0)
# affect_results <- rbind(AH,AL,AN,AP)

```



**Graafikud**
----------------
```{r echo = FALSE}
#Valents
ggplot(subjects_trials, aes(x_percent))+
        geom_density(aes(group=trial, color= trial), size = 1)+
        ggtitle("Valents")
#Intensiivsus
ggplot(subjects_trials, aes(y_percent))+
        geom_density(aes(group=trial, color=trial), size = 1)+
        ggtitle("Intensiivsus")
# Kasutatud andmestik - hinnangute keskmised
plot1 <- ggplot(affect_results, aes(trial, Vkeskmine, fill = trial))+
        geom_boxplot()+
        ggtitle("Valentsi hinnangud kategooriate lõikes")+  
        theme(plot.title = element_text(lineheight=.8, face="bold"))
print(plot1)
plot2 <- ggplot(affect_results, aes(trial, Ikeskmine, fill = trial))+
        geom_boxplot()+
        ggtitle("Intensiivsuse hinnnangud kategooriate lõikes")+
        theme(plot.title = element_text(lineheight=.8, face="bold"))
print(plot2)
```



**VALENTS**
------------
**Kategooriate võrdlemine - ANOVA**  
```{r, echo = FALSE, eval = FALSE}
#Eelduste testimine: - http://gchang.people.ysu.edu/r/R_OneWayANOVA.pdf
# The response is normally distributed
# Variance is similar within different groups
# The data points are independent
#1. Check normality assumption in One Way ANOVA
# Kui p vÃ¤Ã¤rtus on Ã¼le 0.05 siis vÃµib vÃ¤ita, et tegemist on normaaljaotusega
subjects_trials <- droplevels(subjects_trials)
by(subjects_trials$x_percent, subjects_trials$trial, shapiro.test)
by(subjects_trials$y_percent, subjects_trials$trial, shapiro.test)
# Sfäärilisuse nõue on rikutud. Ükski kategooria ei ole nomraaljoatuslik.

# 2. Check Homogeneity of variances assumption for One Way ANOVA
bartlett.test(subjects_trials$x_percent, subjects_trials$trial)

# # ANOVA - mitme sõltumatu muutujaga
# aov_val <- aov(x_percent~gender*trial*ID*age.cat*skill, data = subjects_trials)
# summary(aov_val)

# ANOVA eeldused ei ole tÃ¤idetud. 
```

```{r}
# One Way ANOVA - valents
aov_val <- aov(Vkeskmine~trial, data = affect_results)
summary(aov_val)
#Millised grupid erinevad valnetsi poolest:
tkV <- TukeyHSD(aov_val)
tkV
plot(tkV, las = 1,cex.axis = 0.5)
```


**INTENSIIVSUS**
------------
**Kategooriate võrdlemine - ANOVA**
```{r}
# One Way ANOVA - intensiivsus
aov_int<- aov(Ikeskmine~trial, data = affect_results)
summary(aov_int)
#Millised grupid erinevad intensiivsuse poolest:
tkI <- TukeyHSD(aov_int)
tkI
plot(tkI, las = 1,cex.axis = 0.5)
```


```{r, echo = FALSE, eval = F}
# **Kruskal-Wallis Test**  
# -----------------------
# http://yatani.jp/teaching/doku.php?id=hcistats:kruskalwallis  
# **Valents**  
kruskal.test(x_percent ~ trial, data = subjects_trials)
pairwise.wilcox.test(subjects_trials$x_percent, subjects_trials$trial, p.adj="bonferroni", exact=F)
```

```{r, echo = FALSE,eval = F}
# **Intensiivsus**  
kruskal.test(y_percent ~ trial, data = subjects_trials)
pairwise.wilcox.test(subjects_trials$y_percent, subjects_trials$trial, p.adj="bonferroni", exact=F)
```

```{r, echo = FALSE,eval = F}
# Randomized Block Design - Friedman Test 
# friedman.test(y_percent ~ trial|gender, data = subjects_trials)
# friedman.test(subjects_trials$y_percent, subjects_trials$trial, subjects_trials$gender)
# where y are the data values, A is a grouping factor
# and B is a blocking factor
```


**Tulemuste kuvamine**  
-----------------------
```{r, echo =FALSE}
plotall <- ggplot(affect_results, aes(ZVkeskmine, ZIkeskmine, fill = stimuli))+
        geom_point()+
        ylim(-1.5,1.5)+
        xlim(-1.5,1.5)+ 
#         geom_errorbarh(aes(xmin=ZVkeskmine-SD.VZ/2, xmax = ZVkeskmine +SD.VZ/2))+
#         geom_errorbar(aes(ymin=ZIkeskmine-SD.IZ/2,ymax=ZIkeskmine+SD.IZ/2))+
        ggtitle("Kategooriate keskmised tulemused")+
        labs(x = "Valents(z)", y= "Intensiivsus(z)")+
        theme(legend.position = "none")+
        facet_wrap(~trial)
plotall
```

**Kategooria - positiivne**
---------------------------
```{r, echo = FALSE, plotly = TRUE, warning=F, message= F}
Sys.setenv("plotly_username"="kolnesm")
Sys.setenv("plotly_api_key"="s14ijhulml")
# head(affect_results)
# plotin sÃµnade paiknevused raamistikus:
#library(plotly)
#Sys.setenv("plotly_username"="kolnesm")
#Sys.setenv("plotly_api_key"="s14ijhulml")
#Positiivsed kategooria sÃµnad
trialPos <- filter(affect_results, trial == "Positive")

plotP1 <- ggplot(trialPos, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+
        geom_point()+
        ylim(-1,101)+
        xlim(-101,101)+
        ggtitle("Postiivsed")+
        labs(x = "Valents", y= "Intensiivsus")+
ggplotly(filename="ggplot2-docs/geom_1")
```

**Kategooria - negatiivne**
----------------------------
```{r, echo = FALSE, plotly = TRUE, warning=F, message= F}
trialNeg <- filter(affect_results, trial == "Negative")


plotN1 <- ggplot(trialNeg, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+
        geom_point()+
        ylim(-1,101)+
        xlim(-101,101)+
        ggtitle("Negatiivsed")+
        labs(x = "Valents", y= "Intensiivsus")

ggplotly(filename="ggplot2-docs/geom_2")

```

**Kategooria - tugev**
---------------------
```{r, echo = FALSE, plotly = TRUE, warning=F, message= F}
trialH <- filter(affect_results, trial == "High")
# plotH <- ggplot(trialH, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+
#         geom_point()+
#         ylim(-1,101)+
#         xlim(-101,101)+
#         ggtitle("Tugevad sÃµnad")+
#         labs(x = "Valents", y= "Intensiivsus")


plotH1 <- ggplot(trialH, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+geom_point()+
        ylim(-1,101)+
        xlim(-101,101)+ 
        labs(title = "Tugevad")+xlab("Valents")+
        ylab("Intensiivsus")
ggplotly(filename="ggplot2-docs/geom_3")
```

**Kategooria - madal**  
---------------------
```{r, echo = FALSE, plotly = TRUE, warning=F, message= F}
trialL <- filter(affect_results, trial == "Low")
trialL <- trialL[order(trialL$SD.VZ),]
# ggplot(trialL, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+
#         geom_point(size=3)+
#         geom_errorbarh(aes(xmin=Vkeskmine-SD.V/2, xmax = Vkeskmine +SD.V/2))+
#         geom_errorbar(aes(ymin=Ikeskmine-SD.I/2,ymax=Ikeskmine+SD.I/2))+
# #         ylim(-1,101)+
# #         xlim(-101,101)+
#         ggtitle("NÃµrgad sÃµnad")+
#         labs(x = "Valents", y= "Intensiivsus")
# library(plotly)
# Sys.setenv("plotly_username"="kolnesm")
# Sys.setenv("plotly_api_key"="s14ijhulml")
# Sys.setenv("plotly_domain"="https://plot.ly/~kolnesm/folder/home")
# (gg <- ggplotly(ggg))
plotL2 <- ggplot(trialL, aes(Vkeskmine, Ikeskmine, fill = stimuli, col = stimuli))+
        geom_point()+
        ylim(-1,101)+
        xlim(-101,101)+
        ggtitle("Madalad")+
        labs(x = "Valents", y= "Intensiivsus")+
        theme(legend.position = "right")
ggplotly(plotL2, filename="ggplot2-docs/geom_4")
```

```{r, echo =FALSE}
# library(xlsx)
# write.xlsx(affect_results, "C:/Users/Martin/Documents/affect_results.xlsx")
```


